# python-imageio

A Python library that provides an easy interface to read and write a wide range of image data

https://github.com/imageio/imageio

<br>

How to clone this repository:

```
git clone https://gitlab.com/azul4/content/multimedia/tartube/python-imageio.git
```

